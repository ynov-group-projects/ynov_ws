import {Db, MongoClient} from "mongodb";

export class WsConnectMongoDb {
    mongoClient: MongoClient;
    db: string;
    constructor(url: string, db: string) {
        this.mongoClient = new MongoClient(url);
        this.db = db;
    }

    async connect(): Promise<Db> {
        try {
            await this.mongoClient.connect();

            return this.mongoClient.db(this.db);
        } catch (e) {
            throw new Error("Error occured when connecting to the database : " + e);
        }
    }

    async disconnect(): Promise<void> {
       try {
           await this.mongoClient.close();
       } catch (e) {
           throw new Error("Error occured when disconnecting from the database : " + e);
       }
    }
}

