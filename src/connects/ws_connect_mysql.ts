import { createConnection, Connection } from 'mysql2';
import * as fs from "fs";

const dbConfig = {
    host: 'ynov-web-service-test-sso.a.aivencloud.com',
    user: 'avnadmin',
    password: 'AVNS_zM4itAeMHItlWtAEmW4',
    database: 'defaultdb',
    port: 15427,
    ssl: {
        rejectUnauthorized: true,
        ca: fs.readFileSync("src/dbConfigMysql.ssl.ca").toString(),
    },
};

// Create a connection
const connection: Connection = createConnection(dbConfig);

export default connection;
