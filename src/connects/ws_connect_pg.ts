import {Pool, PoolClient} from "pg";

const pg = require('pg');

const config = {
    user: "avnadmin",
    password: "AVNS_q_M-Ehfq9ntD51wl0Z7",
    host: "ws-connect-pg-strackzdev.a.aivencloud.com",
    port: 21518,
    database: "defaultdb",
    ssl: {
        rejectUnauthorized: true,
        ca: `-----BEGIN CERTIFICATE-----
MIIEQTCCAqmgAwIBAgIUXzoORcGjh/gC1ScHlhAYGjX3q7kwDQYJKoZIhvcNAQEM
BQAwOjE4MDYGA1UEAwwvOTFkNGZjNmMtODY3YS00NTg2LTliMzgtZThkMjA4NzI3
NTZmIFByb2plY3QgQ0EwHhcNMjQwMzI5MDg1MDA1WhcNMzQwMzI3MDg1MDA1WjA6
MTgwNgYDVQQDDC85MWQ0ZmM2Yy04NjdhLTQ1ODYtOWIzOC1lOGQyMDg3Mjc1NmYg
UHJvamVjdCBDQTCCAaIwDQYJKoZIhvcNAQEBBQADggGPADCCAYoCggGBALhFkjx1
u4FB5hnQGE2aG0xz/Uoizk+XVB+gNP9uaeEnl7w4t0ia1zXY6B+Ksnuhob/+Aa6O
9IqaHq6uz9vDQvwHcvnUIiXSzKLzg0CeEDxwwExeD7KH/RYiyVgHzufQ1PJk4Alh
HjRuOq5A+FxFikLPEf9qxf4J/xnLFKBz/EW8gGKmkeOzsaWHzp7K75EtBDUsGYPw
zpvqxJZdGgFs7hSZ3uTfRiXKeV3yBCqnERK36u3FqiAEML3Ci4rd/8N8q3b/JpEi
MfSsBbk6+le9iYfZiy/txy6uhKXDJuRICPRimFVJjdBvCXeXwbw7UZd6um5wSUqU
WMuCQWitU3elXXRz+31mIg/FOBGDYIPZqVdBqlYtI1qejQG6x5H5gDtonjmxzAZS
/QmUJS2b8p3z7v917iWsJnsgDVsh1p4ttRcNLE55GUUPqiotqOoUR12/qYE53P5h
AEr0ODtsaogsoVgaY8Mjf7SANqjXb4Qtbs2pWglgGo6XOKD/Whh2NY1LyQIDAQAB
oz8wPTAdBgNVHQ4EFgQUg23k1vCB8ba6tk3pTn4Lj9EB3y0wDwYDVR0TBAgwBgEB
/wIBADALBgNVHQ8EBAMCAQYwDQYJKoZIhvcNAQEMBQADggGBAKnvMFQDxQp0GYj4
JbXRMGGNq/EEcorfdb3lflkrNBRJMXxKrcRSBUaXlb7n5yasTJRUvnn0Tpwfek23
3Treko/PKQvncfd6pAHU2bSmGAuLLxSselyKTr3wMdwIpDSRCuiYz5r8VpijhC0Q
a5vdVkrIB1jZnKaD9BpSlJzQrPOcXKwf6ilDfcBG6XBcn+UEaDsfagRgJoYKYWtP
5e1UPZ2Iw3KxzEy2j9k679Zk/YdLF084OpbDiwSgGYTt0LE0Q/KqpJUQUfFKo3Qs
q1n5LFc9dg3wv7wFIrb1jLjBgvSZHt7w+A8BXtttT9h8mcqQsiiAlXTwKC5533UP
PMyee0Q0VoteL1Si2L/sogZEilpMx0L/guCitO7SHMrWLjdTyhomSgSsXd5yhFr3
gUnx0r+ckdp7MMaSaq1qoejSRsVYQQNlsnZba1bVg+daYNctVgzMmNN9pscu4L/T
RglCxuVwXIQjs5Ga7xB6ldfcYUsBn5Qbn9+fFyc9r9WCHb/A4w==
-----END CERTIFICATE-----`,
    },
};

export class WsConnectPg {
    private pool: Pool;

    constructor() {
        this.pool = new Pool(config);
    }

    async connect(): Promise<PoolClient> {
        try {
            return await this.pool.connect();
        } catch (error: any) {
            console.error('Error connecting to database:', error.message);
            throw error;
        }
    }

    async disconnect() {
        try {
            await this.pool.end();
            console.log('Disconnected from database.');
        } catch (error: any) {
            console.error('Error disconnecting from database:', error.message);
            throw error;
        }
    }
}