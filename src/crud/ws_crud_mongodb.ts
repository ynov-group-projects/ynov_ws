import {Db, DeleteResult, Document, UpdateResult} from "mongodb";

export class WsCrudMongoDb {
    db: Db
    constructor(db: Db) {
        this.db = db;
    }

    async create(collection: string, document: Document): Promise<Document> {
        try {
            return await this.db.collection(collection).insertOne(document);
        } catch (e) {
            throw new Error("Error occured when creating document : " + e);
        }
    }

    async getAll(collection: string): Promise<Document[]> {
        try {
            return await this.db.collection(collection).find({}).toArray();
        } catch (e) {
            throw new Error("Error occured when getting documents : " + e);
        }
    }

    async getAllByQuery(collection: any, query: any): Promise<Document[]> {
        try {
            return await this.db.collection(collection).find(query).toArray();
        } catch (e) {
            throw new Error("Error occured when getting documents : " + e);
        }
    }

    async update(collection: any, query: any, set: any): Promise<UpdateResult> {
        try {
            return await this.db.collection(collection).updateOne(query, { $set: set });
        } catch (e) {
            throw new Error("Error occured when updating a document : " + e);
        }
    }

    async remove(db: any, collection: any, filter: any): Promise<DeleteResult> {
        try {
            return await this.db.collection(collection).deleteMany(filter);
        } catch (e) {
            throw new Error("Error occured when deleting a document : " + e);
        }
    }
}