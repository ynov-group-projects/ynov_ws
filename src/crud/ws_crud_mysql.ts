import connection from '../connects/ws_connect_mysql';

import { QueryError, FieldPacket } from 'mysql2';

export function readMaskData(callback: (err: QueryError | null, result: any[], fields: FieldPacket[]) => void) {
    const sql = 'SELECT * FROM ws_masks';
    connection.query(sql, callback);
}

export function insertMaskData(name: string, description: string, maskJson: string, callback: (err: QueryError | null, result: any, fields: FieldPacket[]) => void) {
    const sql = 'INSERT INTO ws_masks (name, description, mask_json) VALUES (?, ?, ?)';
    connection.query(sql, [name, description, maskJson], callback);
}

export function updateMaskData(id: number, name: string, description: string, maskJson: string, callback: (err: QueryError | null, result: any, fields: FieldPacket[]) => void) {
    const sql = 'UPDATE ws_masks SET name = ?, description = ?, mask_json = ? WHERE id = ?';
    connection.query(sql, [name, description, maskJson, id], callback);
}

export function deleteMaskData(id: number, callback: (err: QueryError | null, result: any, fields: FieldPacket[]) => void) {
    const sql = 'DELETE FROM ws_masks WHERE id = ?';
    connection.query(sql, [id], callback);
}