import {WsEntryModel} from "../model/ws_entry.model";
import {WsConnectPg} from "../connects/ws_connect_pg";

export class WsCrudPg {
    private client;
    private db;

    constructor(private wsConnectPg: WsConnectPg) {
        this.client = wsConnectPg;
        this.db = this.client.connect();
    }

    async createEntry(entry: WsEntryModel): Promise<number> {
        try {
            const queryText = 'INSERT INTO ws_entries (id_mask, entry_json) VALUES ($1, $2) RETURNING id';
            const values: any= [entry.id_mask, entry.entry_json];
            const result = await (await this.db).query(queryText, values);
            return result.rows[0].id;
        } catch (error: any) {
            console.error('Error creating entry:', error.message);
            throw error;
        }
    }


    async readEntry(id: number): Promise<WsEntryModel | null> {
        try {
            const queryText = 'SELECT * FROM ws_entries WHERE id = $1';
            const values: any = [id];
            const result = await (await this.db).query(queryText, values);
            return result.rows[0] || null;
        } catch (error: any) {
            console.error('Error reading entry:', error.message);
            throw error;
        }
    }

    async readAllEntry(): Promise<WsEntryModel | null> {
        try {
            const queryText = 'SELECT * FROM ws_entries';
            const result = await (await this.db).query(queryText);
            return result.rows[0] || null;
        } catch (error: any) {
            console.error('Error reading entry:', error.message);
            throw error;
        }
    }

    async updateEntry(entry: WsEntryModel): Promise<void> {
        try {
            const queryText = 'UPDATE ws_entries SET id_mask = $1, entry_json = $2 WHERE id = $3';
            const values: any = [entry.id_mask, entry.entry_json, entry.id];
            await (await this.db).query(queryText, values);
        } catch (error: any) {
            console.error('Error updating entry:', error.message);
            throw error;
        }
    }

    async deleteEntry(id: number): Promise<void> {
        const db = await this.client.connect();
        try {
            const queryText = 'DELETE FROM ws_entries WHERE id = $1';
            const values: any = [id];
            await (await this.db).query(queryText, values);
        } catch (error: any) {
            console.error('Error deleting entry:', error.message);
            throw error;
        }
    }
}