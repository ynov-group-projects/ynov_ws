import express, { Request, Response } from "express";
import {WsConnectPg} from "./connects/ws_connect_pg";
import {WsDbInitPg} from "./inits/ws_dbinit_pg";
import {WsCrudPg} from "./crud/ws_crud_pg";
import connection from './connects/ws_connect_mysql';
import entriesRouter from "./router/entries";
const app = express();
/*import createWsMasksTableSQL from './inits/ws_dbinit_mysql';
import { readMaskData, insertMaskData, updateMaskData, deleteMaskData } from './crud/ws_crud_mysql';


import {WsConnectMongoDb} from "./connects/ws_connect_mongodb";
import {WsCrudMongoDb} from "./crud/ws_crud_mongodb";


const WS_CONNECTION_PG: WsConnectPg = new WsConnectPg();

const WS_DBINIT_PG: WsDbInitPg = new WsDbInitPg(WS_CONNECTION_PG);
const WS_PG_CRUD: WsCrudPg = new WsCrudPg(WS_CONNECTION_PG);

async function runCrud() {
    try {
        const newEntryId = await WS_PG_CRUD.createEntry({
            id_mask: 5,
            entry_json: "create"
        });


        await WS_PG_CRUD.readEntry(newEntryId).then((data) => {
            console.log("------[ CREATE ENTRY POSTGRES ]------")
            console.log(data)
        });

        await WS_PG_CRUD.updateEntry({
            id: newEntryId,
            id_mask: 50,
            entry_json: "update"
        });

        await WS_PG_CRUD.readEntry(newEntryId).then((data) => {
            console.log("------[ UPDATE ENTRY POSTGRES ]------")
            console.log(data)
        });
    } catch (error: any) {
        console.error('Error:', error.message);
    }
}

runCrud().then();


// MONGO DB
function runCrudMongoDb() {
    const wsConnectMongoDb: WsConnectMongoDb = new WsConnectMongoDb("mongodb+srv://cyrilnguyen:79dFEwyDd5hUYXWQ@ynov-ws.lairi9j.mongodb.net/?retryWrites=true&w=majority&appName=ynov-ws", "ynov_ws");

    wsConnectMongoDb.connect().then(async db => {
        const wsCrudMongoDb: WsCrudMongoDb = new WsCrudMongoDb(db);

        await wsCrudMongoDb.create("ws_masks", {
            description: "description 1",
            name: "name 3",
            mask_json: "mask_json 1"
        }).then(value => {
            console.log("-- MONGO - CREATE");
            console.log(value);
        });

        await wsCrudMongoDb.getAll("ws_masks").then(value => {
            console.log("-- MONGO - GET ALL");

            value.forEach(document => {
                console.log(document._id);
                console.log(document.name);
                console.log(document.description);
                console.log("---------------------");
            })
        });

        await wsCrudMongoDb.getAllByQuery("ws_masks", {
            name: "name 2"
        }).then(value => {
            console.log("-- MONGO - GET ALL BY QUERY");

            value.forEach(document => {
                console.log(document._id);
                console.log(document.name);
                console.log(document.description);
                console.log("---------------------");
            })
        });

        await wsCrudMongoDb.update("ws_masks", {
                name: "name 1",
            }, {
                description: "description updated 1"
            }
        ).then(value => {
            console.log("-- MONGO - UPDATE BY QUERY");
            console.log(value);
        });

        await wsCrudMongoDb.remove(db, "ws_masks", {
            name: "name 1"
        }).then(value => {
            console.log("-- MONGO - DELETE BY FILTER");
            console.log(value);
        });

    });

    wsConnectMongoDb.disconnect().then();
}

runCrudMongoDb();*/


// Connect to the database
// connection.connect((err) => {
    /*if (err) {
        console.error('Error connecting to database:', err);
        // Handle the error
        process.exit(1); // Exit the process if unable to connect to the database
    }
    console.log('Connected to MySQL database');

    // Create ws_masks table if not exists
    connection.query(createWsMasksTableSQL, function (err, result) {
        if (err) {
            console.error("Error creating ws_masks table:", err);
        } else {
            console.log("ws_masks table created successfully");
        }
    });


    // Example of using insertMaskData function
    const name = "Example Mask";
    const description = "Example description";
    const maskJson = "{\"key\": \"value\"}"; // Example JSON string
    insertMaskData(name, description, maskJson, (err: any, result: any) => {
        if (err) {
            console.error("Error inserting mask data:", err);
        } else {
            console.log("Mask data inserted successfully");
        }
    });

    // Example of using readMaskData function
    readMaskData((err: any, result: any) => {
        if (err) {
            console.error("Error reading mask data:", err);
        } else {
            console.log("Mask data:", result);
        }
    });

    // Example of using updateMaskData function
    const maskIdToUpdate = 1; // Example mask ID to update
    const updatedName = "Updated Mask";
    const updatedDescription = "Updated description";
    const updatedMaskJson = "{\"key\": \"updated value\"}"; // Example updated JSON string
    updateMaskData(maskIdToUpdate, updatedName, updatedDescription, updatedMaskJson, (err: any, result: any) => {
        if (err) {
            console.error("Error updating mask data:", err);
        } else {
            console.log("Mask data updated successfully");
        }
    });

    // Example of using deleteMaskData function
    const maskIdToDelete = 1; // Example mask ID to delete
    deleteMaskData(maskIdToDelete, (err: any, result: any) => {
        if (err) {
            console.error("Error deleting mask data:", err);
        } else {
            console.log("Mask data deleted successfully");
        }
    });
    // Example of using readMaskData function
    readMaskData((err: any, result: any) => {
        if (err) {
            console.error("Error reading mask data:", err);
        } else {
            console.log("Mask data:", result);
        }
    });*/

//});
const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`Serveur démarré sur le port ${port}`);
})
app.use("/", entriesRouter);