import connection from "../connects/ws_connect_mysql";

const createWsMasksTableSQL = `
    CREATE TABLE IF NOT EXISTS ws_masks (
        id INT AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(255) NOT NULL,
        description VARCHAR(255),
        mask_json JSON
    )
`;

// SQL query to create the ws_entries table
const createWsEntriesTableSQL = `
    CREATE TABLE IF NOT EXISTS ws_entries (
        id INT AUTO_INCREMENT PRIMARY KEY,
        id_mask INT NOT NULL,
        entry_json JSON,
        FOREIGN KEY (id_mask) REFERENCES ws_masks(id)
    )
`;

connection.query(createWsMasksTableSQL, function (err, result) {
    if (err) {
        console.error("Error creating ws_masks table:", err);
    } else {
        console.log("ws_masks table created successfully");
    }
});

connection.query(createWsEntriesTableSQL, function (err, result) {
    if (err) {
        console.error("Error creating ws_entries table:", err);
    } else {
        console.log("ws_entries table created successfully");
    }
});

export default createWsMasksTableSQL
