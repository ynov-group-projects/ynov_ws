import {WsConnectPg} from "../connects/ws_connect_pg";

export class WsDbInitPg {
    private WS_CONNECTION_PG: WsConnectPg;

    constructor(WS_CONNECTION_PG: WsConnectPg) {
        this.WS_CONNECTION_PG = WS_CONNECTION_PG;
        this.initTableCreation().finally(() => console.log("Tables Created !!"))
    }

    private async initTableCreation(): Promise<void> {
        const client = await this.WS_CONNECTION_PG.connect();
        try {
            let queryWsMasks = await client.query(`
                CREATE TABLE IF NOT EXISTS ws_masks (
                    id SERIAL PRIMARY KEY,
                    name VARCHAR(255) NOT NULL,
                    description TEXT NOT NULL,
                    mask_json TEXT NOT NULL
                )
            `);

            let queryWsEntries = await client.query(`
                CREATE TABLE IF NOT EXISTS ws_entries (
                    id SERIAL PRIMARY KEY,
                    id_mask SERIAL NOT NULL,
                    entry_json TEXT NOT NULL
                )
            `);

            let existingDatabaseTables = await client.query(`
                SELECT * FROM pg_catalog.pg_tables;
            `)

            const ourPersistedTables = existingDatabaseTables.rows
                .map((obj: any) => obj.tablename)
                .filter((tablename: string) => tablename.includes("ws"));

            console.log("Table creation : " + ourPersistedTables)
        } finally {
            await this.WS_CONNECTION_PG.disconnect();
        }
    }
}