export interface WsEntryModel {
    id?: number;
    id_mask: number;
    entry_json: string;
}