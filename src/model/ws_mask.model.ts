export interface WsMaskModel {
    id: number;
    name: string;
    description: string;
    mask_json: string;
}