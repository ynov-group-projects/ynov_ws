import express from "express";
import { Request, Response } from "express";
import { WsCrudMongoDb } from "../crud/ws_crud_mongodb";
import { WsConnectMongoDb } from "../connects/ws_connect_mongodb";
import {WsCrudPg} from "../crud/ws_crud_pg";
import {WsConnectPg} from "../connects/ws_connect_pg";
import {ObjectId} from "mongodb";

const WS_CONNECTION_PG: WsConnectPg = new WsConnectPg();
const WS_PG_CRUD: WsCrudPg = new WsCrudPg(WS_CONNECTION_PG);

const router = express.Router();
const wsConnectMongoDb: WsConnectMongoDb = new WsConnectMongoDb("mongodb+srv://cyrilnguyen:79dFEwyDd5hUYXWQ@ynov-ws.lairi9j.mongodb.net/?retryWrites=true&w=majority&appName=ynov-ws", "ynov_ws")
// Connect to MongoDB and define routes
wsConnectMongoDb.connect().then(async (db) => {
	const wsCrudMongoDb: WsCrudMongoDb = new WsCrudMongoDb(db);

	router.get("/api/v1/masks", async (req: Request, res: Response) => {
		try {
			const entries = await wsCrudMongoDb.getAll("ws_masks");
			return res.json({ success: true, entries });
		} catch (error) {
			res.status(500).json({ success: false, error: error });
		}
	});

	router.get("/api/v1/masks/:id", async (req: Request, res: Response) => {
		const objectId = new ObjectId(req.params.id);
		try {
			const entries = await wsCrudMongoDb.getAllByQuery("ws_masks", { _id: objectId });
			return res.json({ success: true, entries });
		} catch (error) {
			res.status(500).json({ success: false, error: error });
		}
	});

	router.post("/api/v1/masks", async (req: Request, res: Response) => {
		try {
			const entry = await wsCrudMongoDb.create("ws_masks", req.body);
			return res.json({ success: true, entry });
		} catch (error) {
			res.status(500).json({ success: false, error: error });
		}
	});

	router.put("/api/v1/masks/:id", async (req: Request, res: Response) => {
		const objectId = new ObjectId(req.params.id);
		try {
			const entry = await wsCrudMongoDb.update("ws_masks", { _id: objectId }, req.body);
			return res.json({ success: true, entry });
		} catch (error) {
			res.status(500).json({ success: false, error: error });
		}
	});

	router.delete("/api/v1/masks/:id", async (req: Request, res: Response) => {
		const objectId = new ObjectId(req.params.id);
		try {
			const entry = await wsCrudMongoDb.remove(db, "ws_masks", { _id: objectId });
			return res.json({ success: true, entry });
		} catch (error) {
			res.status(500).json({ success: false, error: error });
		}
	});
});

export default router;
